package com.example.mlww.jakdojadedemo.model;

import java.util.List;

public class Station {

    private Geometry geometry;
    private Long id;
    private String type;
    private Properties properties;

    public class Geometry {

        List<Double> coordinates;
        String type;

        public List<Double> getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(List<Double> coordinates) {
            this.coordinates = coordinates;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public class Properties {

        private String free_racks;
        private String bikes;
        private String label;
        private String bike_racks;
        private String updated;

        public String getFree_racks() {
            return free_racks;
        }

        public void setFree_racks(String free_racks) {
            this.free_racks = free_racks;
        }

        public String getBikes() {
            return bikes;
        }

        public void setBikes(String bikes) {
            this.bikes = bikes;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getBike_racks() {
            return bike_racks;
        }

        public void setBike_racks(String bike_racks) {
            this.bike_racks = bike_racks;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
