package com.example.mlww.jakdojadedemo;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.example.mlww.jakdojadedemo.model.Station;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Station station;
    LocationManager locationManager;
    private TextView stationId;
    private TextView stationName;
    private TextView availableBicycles;
    private TextView availableRacks;
    private TextView addresses;
    private TextView distanceFromUser;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        String provider = locationManager.getBestProvider(new Criteria(), false);
                        //Request location updates:
                        locationManager.requestLocationUpdates(provider, 400, 1, (LocationListener) this);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        checkLocationPermission();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String json = bundle.getString("station"); // getting the model from MainActivity send via extras
            station = new Gson().fromJson(json, Station.class);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setUIViews();

        Geocoder geocoder;
        List<Address> fromLocation = null;
        geocoder = new Geocoder(getApplicationContext(), Locale.forLanguageTag("PL"));

        Double latitude = station.getGeometry().getCoordinates().get(1);
        Double longitude = station.getGeometry().getCoordinates().get(0);

        try {
            fromLocation = geocoder.getFromLocation(latitude, longitude, 1);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));
        float distanceInMeters = calculateDistance(latitude, longitude, location);

        String address = fromLocation.get(0).getAddressLine(0);

        Double distance = (double) distanceInMeters;

        int roundedDistance = roundDown(distance, 4);

        convertViews(address, roundedDistance);

    }

    private float calculateDistance(Double latitude, Double longitude, Location location) {
        double userLatitude = location.getLatitude();
        double userLongitude = location.getLongitude();

        Location userLocation = new Location("");
        userLocation.setLatitude(userLatitude);
        userLocation.setLongitude(userLongitude);

        Location markerLocation = new Location("");
        markerLocation.setLatitude(latitude);
        markerLocation.setLongitude(longitude);

        return userLocation.distanceTo(markerLocation);
    }

    private void convertViews(String address, int i) {
        distanceFromUser.setText(Integer.valueOf(i) + "m");
        addresses.setText(address);
        stationName.setText(station.getProperties().getLabel());
        stationId.setText(station.getId().toString());
        String free_racks = station.getProperties().getFree_racks();
        availableRacks.setText(free_racks);
        String bikes = station.getProperties().getBikes();
        availableBicycles.setText(bikes);

        if (Integer.valueOf(free_racks) < 4) {
            availableRacks.setTextColor(getResources().getColor(R.color.light_red));
        } else if(Integer.valueOf(free_racks) > 15) {
            availableRacks.setTextColor(getResources().getColor(R.color.light_green));
        } else {
            availableRacks.setTextColor(getResources().getColor(R.color.dark_grey));
        }

        if (Integer.valueOf(bikes) < 4) {
            availableBicycles.setTextColor(getResources().getColor(R.color.light_red));
        } else if(Integer.valueOf(bikes) > 15) {
            availableBicycles.setTextColor(getResources().getColor(R.color.light_green));
        } else {
            availableBicycles.setTextColor(getResources().getColor(R.color.dark_grey));

        }
    }

    private void setUIViews() {
        availableBicycles = findViewById(R.id.available_bikes);
        availableRacks = findViewById(R.id.available_places);
        stationId = findViewById(R.id.station_id);
        stationName = findViewById(R.id.station_name);
        addresses = findViewById(R.id.address);
        distanceFromUser = findViewById(R.id.distance_from_user);
    }

    int roundDown(double number, double place) {
        double result = number / place;
        result = Math.floor(result);
        result *= place;
        return (int)result;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        Double latitude = station.getGeometry().getCoordinates().get(1);
        Double longitude = station.getGeometry().getCoordinates().get(0);
        LatLng position = new LatLng(latitude, longitude);

        MarkerOptions markerOptions = new MarkerOptions().position(position).title(station.getProperties().getLabel());
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_bicycle));



        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
    }
}
