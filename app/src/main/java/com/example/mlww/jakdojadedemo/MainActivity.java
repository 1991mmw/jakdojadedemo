package com.example.mlww.jakdojadedemo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mlww.jakdojadedemo.model.Station;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.round;

public class MainActivity extends AppCompatActivity {

    private ListView listBicycleStations;
    private final String URL_TO_HIT = "http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listBicycleStations = findViewById(R.id.listBicycleStations);

        new JSONTask().execute();
    }

    public class JSONTask extends AsyncTask<String, String, List<Station>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Station> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            List<Station> stationList = new ArrayList<>();

            try {
                URL url = new URL(URL_TO_HIT);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                JSONArray parentArray = parentObject.getJSONArray("features");


                Gson gson = new Gson();
                for (int j = 0; j < parentArray.length(); j++) {
                    JSONObject finalObject = parentArray.getJSONObject(j);

                    Station station = gson.fromJson(finalObject.toString(), Station.class);
                    stationList.add(station);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return stationList;
        }

        @Override
        protected void onPostExecute(final List<Station> result) {
            super.onPostExecute(result);
            if (result != null) {
                StationAdapter adapter = new StationAdapter(getApplicationContext(), R.layout.row, result);
                listBicycleStations.setAdapter(adapter);

                listBicycleStations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Station station = result.get(position); // getting the model
                        Intent intent = new Intent(MainActivity.this, MapActivity.class);
                        intent.putExtra("station", new Gson().toJson(station));
                        startActivity(intent);
                    }
                });

            } else {
                Toast.makeText(getApplicationContext(), "Nie można pobrać dane z serwera, proszę sprawdzić url", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class StationAdapter extends ArrayAdapter {

        private List<Station> stationList;
        private int resource;
        private LayoutInflater inflater;


        public StationAdapter(Context context, int resource, List<Station> objects) {
            super(context, resource, objects);
            stationList = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(resource, null);

                getUIViews(convertView, holder);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            //get address by Lat and Long
            Geocoder geocoder;
            List<Address> fromLocation = null;
            geocoder = new Geocoder(getContext(), Locale.forLanguageTag("PL"));

            Double latitude = stationList.get(position).getGeometry().getCoordinates().get(1);
            Double longitude = stationList.get(position).getGeometry().getCoordinates().get(0);

            try {
                fromLocation = geocoder.getFromLocation(latitude, longitude, 1);// Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
            String address = fromLocation.get(0).getAddressLine(0);

            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            //Get Permission
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            float distanceInMeters = calculateDistance(latitude, longitude, location);


            Double distance = (double) distanceInMeters;

            int i = roundDown(distance, 4);

            convertViews(position, holder, address, i);
            return convertView;
        }

        private float calculateDistance(Double latitude, Double longitude, Location location) {
            double userLatitude = location.getLatitude();
            double userLongitude = location.getLongitude();

            Location userLocation = new Location("");
            userLocation.setLatitude(userLatitude);
            userLocation.setLongitude(userLongitude);

            Location markerLocation = new Location("");
            markerLocation.setLatitude(latitude);
            markerLocation.setLongitude(longitude);

            return userLocation.distanceTo(markerLocation);
        }

        private void getUIViews(View convertView, ViewHolder holder) {
            holder.availableBicycles = convertView.findViewById(R.id.available_bikes);
            holder.availableRacks = convertView.findViewById(R.id.available_places);
            holder.stationId = convertView.findViewById(R.id.station_id);
            holder.stationName = convertView.findViewById(R.id.station_name);
            holder.address = convertView.findViewById(R.id.address);
            holder.distanceFromUser = convertView.findViewById(R.id.distance_from_user);
        }

        private void convertViews(int position, ViewHolder holder, String address, int i) {
            holder.distanceFromUser.setText(Integer.valueOf(i) + "m");
            holder.address.setText(address);
            holder.stationName.setText(stationList.get(position).getProperties().getLabel());
            holder.stationId.setText(stationList.get(position).getId().toString());
            String free_racks = stationList.get(position).getProperties().getFree_racks();
            holder.availableRacks.setText(free_racks);
            String bikes = stationList.get(position).getProperties().getBikes();
            holder.availableBicycles.setText(bikes);

            if (Integer.valueOf(free_racks) < 4) {
                holder.availableRacks.setTextColor(getResources().getColor(R.color.light_red));
            } else if(Integer.valueOf(free_racks) > 15) {
                holder.availableRacks.setTextColor(getResources().getColor(R.color.light_green));
            } else {
                holder.availableRacks.setTextColor(getResources().getColor(R.color.dark_grey));
            }

            if (Integer.valueOf(bikes) < 4) {
                holder.availableBicycles.setTextColor(getResources().getColor(R.color.light_red));
            } else if(Integer.valueOf(bikes) > 15) {
                holder.availableBicycles.setTextColor(getResources().getColor(R.color.light_green));
            } else {
                holder.availableBicycles.setTextColor(getResources().getColor(R.color.dark_grey));

            }
        }

        int roundDown(double number, double place) {
            double result = number / place;
            result = Math.floor(result);
            result *= place;
            return (int)result;
        }


        class ViewHolder {

            //Add Detailed View holders

            private TextView stationId;
            private TextView stationName;
            private TextView availableBicycles;
            private TextView availableRacks;
            private TextView address;
            private TextView distanceFromUser;
        }

    }
}
